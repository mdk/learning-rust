type Point1D<T> = (T,);
type Point2D<T> = (T, T);
type Point3D<T> = (T, T, T);

trait Signed {
    fn abs(&self) -> Self;
}

trait Sqrtable {
    fn sqrt(&self) -> Self;
}

trait Distance<T> {
    fn from_origin(&self) -> T;
}

impl<T: Signed> Distance<T> for Point1D<T> {
    fn from_origin(&self) -> T {
        self.0.abs()
    }
}

impl<T: std::ops::Mul<Output = T> + std::ops::Add<Output = T> + Sqrtable + std::marker::Copy>
    Distance<T> for Point2D<T>
{
    fn from_origin(&self) -> T {
        (self.0 * self.0 + self.1 * self.1).sqrt()
    }
}

fn add_points_3d<T: std::ops::Add<Output = T> + std::marker::Copy>(
    p1: &Point3D<T>,
    p2: &Point3D<T>,
) -> Point3D<T> {
    (p1.0 + p2.0, p1.1 + p2.1, p1.2 + p2.2)
}

// fn largest<T: std::cmp::PartialOrd>(list: &[T]) -> &T { // &[T] is a T iterable.
//}

// The debug trait
// #[derive(Debug)] // This is called an attribute, it's like annotations.
// struct Rectangle {
//     width: u32,
//     height: u32,
// }

// There also exist a std::fmt::Display trait, with a required `fmt` function.

// There is also a #[derive(Clone)], we may put in on almost every
// types, as long as it make sense.

// Copy is just a flag telling "The clone is virtually free".

// We can combine, like #[derive[Clone, Debug)]

fn play_with_generics_and_lifetime() {
    // Generics are fully resolved at compile-time. There's 0 cost at runtime.
    let m1: Point3D<i64> = (10, 20, 30);
    let m2: Point3D<i64> = (1, 2, 3);

    let m3 = add_points_3d(&m1, &m2);
    assert!(m3.0 == 11);
}

// PartialEq implement == and !=
// Eq is a flat telling that a value is equal to self
// So floats implement PartialEq but no Eq
// Many other things can be flagged Eq

// impl<T> c'est cool

// Traits:

// trait Area {
//    fn area(&self) -> u32;  // ends with `;`, it's not implemented.
//    fn is_big(&self) -> bool { self.area() > 5 } // it's implemented !
//}

// impl Area for Rectangle {
//    // Here I have to implement at least all unimplemnted methods from trait.
//}

// From et Into are traits to convert from types to other types.

// Lifetime

// lifetime are annotated using simple quote followed by a letter.
// It looks like parameter types, like <'a>:

fn longest<'a>(x: &'a str, y: &'a str) -> &'a str {
    // L'entrée et la sortie doivent partager les mêmes contraintes de durée de vie.
    // La durée de vie la plus courte est choisie.
    if x.len() > y.len() {
        x
    } else {
        y
    }
}

#[derive(Debug)]
struct IPossesTheString {
    name: String,
}

#[derive(Debug)]
struct IDontPossesIt<'a> {
    // La string doit survivre à la structure
    // 'a : IDontPossesIt instances should live "longer"
    name: &'a str, // than this string.
}

// See elision of lifetimes
// See 'static

fn is_in(v: &Vec<&str>, needle: &str) -> bool {
    for s in v {
        if *s == needle {
            return true;
        }
    }
    false
}

fn _print_first_item<T: std::fmt::Debug>(list: &[T]) {
    // Here list is "an iterable".
    dbg!(&list[0]);
}

struct Fib {
    a: u64,
    b: u64,
}

impl Fib {
    fn new() -> Self {
        Self { a: 1, b: 1 }
    }
}

impl Iterator for Fib {
    type Item = u64;

    fn next(&mut self) -> Option<Self::Item> {
        (self.a, self.b) = (self.b, self.a + self.b);
        Some(self.a)
    }
}

pub fn main() {
    play_with_generics_and_lifetime();
    let string1 = String::from("abcd");
    let string2 = "yux";

    let result = longest(string1.as_str(), string2);
    assert!(result == "abcd");

    let a = IPossesTheString {
        name: "Hello".to_owned(),
    };
    let b = IDontPossesIt { name: "Hello" };
    assert!(a.name == "Hello");
    assert!(b.name == "Hello");

    // lambdas:

    let f = |n1, n2| n1 + n2;
    assert!(f(1, 2) == 3);

    // Vec:

    let v: Vec<f64> = Vec::new();

    assert!(v.get(0) == None);

    let with_initial_values = vec![1, 2, 3];
    assert!(with_initial_values[0] == 1);

    // Vectors are muables:

    let mut v = vec![1, 2, 3];
    v.push(4);
    v.push(5);

    assert!(v[0] == 1);

    let second = v[1];
    assert!(second == 2);

    let v: Vec<&str> = vec!["Hello", "world"];
    assert!(is_in(&v, "Hello"));
    assert!(!is_in(&v, "Pouette"));

    // Slices

    let mut v = vec![1, 2, 3, 4, 5, 6];
    //dbg!(&v);
    //dbg!(v.as_slice());
    let s = &mut v[1..=2]; // "MemoryView"
    s[0] = 0;
    let v = vec![10, 20, 30];
    assert!(v[0] == 10);
    //print_first_item(&v);

    // HashSet

    let mut set = std::collections::HashSet::new();
    set.insert("Hello");
    set.insert("World");
    assert!(set.contains("Hello"));
    assert!(!set.contains("Boo"));

    // HashMap

    let mut dict = std::collections::HashMap::new();
    dict.insert("Key", "Value");
    assert!(dict.contains_key("Key"));
    assert!(dict["Key"] == "Value");

    for (key, value) in &dict {
        assert!(key == &"Key");
        assert!(value == &"Value");
    }

    // Iterators (it's a trait)
    let fib = Fib::new();
    for i in fib {
        if i > 1000 {
            break;
        }
    }
    let fib10 = Fib::new().take(10).collect::<Vec<_>>();
    assert!(fib10[5] == fib10[4] + fib10[3]);
}
