mod bench;
mod concurrency;
mod doc;
mod errors;
mod exercises;
mod jour1;
mod jour2;
mod python;
mod rpg;
mod starwars;
mod tests;

fn main() {
    jour1::main();
    exercises::main();
    jour2::main();
    rpg::main();
    starwars::main();
    tests::main();
    doc::main();
    errors::main();
    concurrency::main();
    python::main();
}
