// Crates
// ======
//
// A library or executable, it has a root module, one of:
// src/main.rs : Contains the main function.
// src/lib.rs : Contains exposed entities as a library.
//
//
// Modules
// =======
//
// A tree of code. Controls visibility.
//
// my_module.rst is "importable" using `mod my_module;`
// In a directory, mod.rs is like __init__.py in Python.
//
// `mod` imports a module (tells the compiler that the file exists).
// `use` imports a thing (fn, struct, ...), like a shortcut.
// use can be used like `use ... as ...`.
// use can be used as `use my_module::{..., ...}`.
// `pub use` imports and make public.
//
//
// Tooling
// =======
//
//     rustup : tool to install all the tools
//         rustup update  // updates all the tools
//
//     cargo new my_project
//     cargo new my_project --lib
//     cargo clean
//     cargo build --release
//     cargo run --release
//     cargo check
//     cargo install <paquet>
//
// In a Cargo.toml there's a package.edition field. It's for retro-compatibility.
//
// There's a [dependencies] dict.
//
//
// Features
// ========
//
// It's for compilation flags
// It's a 'features' table in the toml file:
//
//     [features]
//     my_feat = []
//
// Uses as:
//
//     #[cfg(feature = "my_feat")]
//     fn hello() {}
//
//     #[cfg(not(feature = "my_feat"))]
//     fn hello() {}
//
// There's a special `default` feature.
//
// There's optional dependencies:
//
//     [dependencies]
//     rand = { version = "0.8.5", optiona = true }
//     // rand is only included if feature rand is active
//
//     [features]
//     my_feat = ["rand"] // Activate the optional dependency
//
// Or:
//
//     [dependencies]
//     rand = { version = "0.8.5", features = ["log"] }
//
// Linters
// =======
//
//     cargo clippy
//     cargo clippy --all --all-targets --all-features -- -D warnings
//
// https://rust-lang.github.io/rust-clippy/master/index.html
//
//
// Tests
// =====
//
// See at the end of this file.  Integration tests (not included at
// compile time) can be stored in a tests/ directory near the src/
// directory. They are also ran by `cargo test`.
//
// See also ntest, mockito, proptest.
//
mod my_module {
    #[allow(dead_code)]
    pub fn hello() {
        todo!()
    }

    pub mod nested_module {
        #[allow(dead_code)]
        pub fn bar() {
            todo!()
        }
    }
}

fn add<T: std::ops::Add<Output = T>>(x: T, y: T) -> T {
    x + y
}

pub fn main() {
    //my_module::hello();
    //my_module::nested_module::bar();
    assert!(add(0.1, 0.2) == 0.2 + 0.1);
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn it_works() {
        let x = 5;
        assert!(add(x, x) == 10);
    }
}
