// cargo doc --open
// --open to open the browser.

/// # The doc is in Markdown
///
///
/// - Use `///` for item comments
/// - Use `//!` for module comments
///
/// Examples are doctests:
///
/// ```
/// let d1 = Duration::from_secs(60);
/// ```
///

pub fn main() {}
