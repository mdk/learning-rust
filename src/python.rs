pub fn main() {
    #[link(name = "hello_python")]
    extern "C" {
        fn hello_python();
    }
    unsafe {
        hello_python();
    }
}
