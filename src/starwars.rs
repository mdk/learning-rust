#[derive(Debug, Clone)]
struct Character {
    name: String,
    height: i32,
    mass: i32,
}

impl Character {
    fn bmi(&self) -> f64 {
        (self.mass as f64) / ((self.height as f64) / 100.0).powf(2.0)
    }
}

pub fn main() {
    let characters = vec![
        Character {
            name: "Luke Skywalker".to_owned(),
            height: 172,
            mass: 77,
        },
        Character {
            name: "C-3PO".to_owned(),
            height: 167,
            mass: 75,
        },
        Character {
            name: "R2-D2".to_owned(),
            height: 96,
            mass: 32,
        },
        Character {
            name: "Darth Vader".to_owned(),
            height: 202,
            mass: 136,
        },
        Character {
            name: "Leia Organa".to_owned(),
            height: 150,
            mass: 49,
        },
        Character {
            name: "Owen Lars".to_owned(),
            height: 178,
            mass: 120,
        },
        Character {
            name: "Beru Whitesun lars".to_owned(),
            height: 165,
            mass: 75,
        },
        Character {
            name: "R5-D4".to_owned(),
            height: 97,
            mass: 32,
        },
        Character {
            name: "Biggs Darklighter".to_owned(),
            height: 183,
            mass: 84,
        },
        Character {
            name: "Obi-Wan Kenobi".to_owned(),
            height: 182,
            mass: 77,
        },
    ];

    let mut with_bmi = characters
        .iter()
        .map(|c| (&c.name, c.bmi()))
        .collect::<Vec<_>>();
    with_bmi.sort_by_key(|tpl| ((tpl.1 * 1000.0) as u64));
    //dbg!(&with_bmi);
    let biggest_bmi;
    match with_bmi.last() {
        None => biggest_bmi = "??".to_owned(),
        Some(character) => {
            biggest_bmi = format!("{} has the biggest BMI", character.0);
        }
    }
    assert!(biggest_bmi == "Owen Lars has the biggest BMI");
}
