//! # MPSC channels: Multiple Producers Single Consumer
//!
//! See also the [rayon](https://crates.io/crates/rayon) crate.
//!
//!
//! ## Sync and Send traits
//!
//! They just mark types (no impl needed).
//!
//!
//! ## async/await
//!
//!
//! ## Serialisation
//!
//! Called "Serde" (stands for Serializing, Deserializing).
//!
//!
//!

use std::sync::mpsc;
use std::sync::{Arc, Mutex};
use std::thread;
use std::time::Duration;

use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
struct Point2D {
    x: f64,
    y: f64,
}

fn serialize() {
    let str = r#"
{
    "x": 10.5,
    "y": 11.12
}"#;

    match serde_json::from_str::<Point2D>(str) {
        Ok(point) => {
            assert!(point.x == 10.5);
            assert!(point.y == 11.12);
        }
        Err(e) => {
            println!("{}", e);
        }
    }
}
use nom::{
    bytes::complete::{tag, take_while_m_n},
    combinator::map_res,
    sequence::Tuple,
    IResult,
};

#[derive(Debug, PartialEq)]
pub struct Color {
    pub red: u8,
    pub green: u8,
    pub blue: u8,
}

fn from_hex(input: &str) -> Result<u8, std::num::ParseIntError> {
    u8::from_str_radix(input, 16)
}

fn is_hex_digit(c: char) -> bool {
    c.is_digit(16)
}

fn hex_primary(input: &str) -> IResult<&str, u8> {
    map_res(take_while_m_n(2, 2, is_hex_digit), from_hex)(input)
}

fn hex_color(input: &str) -> IResult<&str, Color> {
    let (input, _) = tag("#")(input)?;
    let (input, (red, green, blue)) = (hex_primary, hex_primary, hex_primary).parse(input)?;
    Ok((input, Color { red, green, blue }))
}

fn parsing() {
    let color = hex_color("#2F14DF");

    match color {
        Ok((_rest, color)) => {
            assert!(color.red == 47);
            assert!(color.green == 20);
            assert!(color.blue == 223);
        }
        Err(e) => {
            dbg!(e);
        }
    }
}

fn using_mpsc() {
    let (tx, rx) = mpsc::channel();
    // tx can be cloned using .clone(), so we can have "Multiple Producers".
    // rx cannot be cloned.
    thread::spawn(move || {
        // Move will just move tx, as only tx is used.
        let val = String::from("Hi");
        tx.send(val).unwrap();
    });

    let received = rx.recv().unwrap();
    assert!(received == "Hi");
}

use std::ops::Deref;

fn using_mutex() {
    // Arc is Atomically Reference Counted
    let counter = Arc::new(Mutex::new(0));
    let mut handles = vec![];

    for _ in 0..10 {
        let counter = Arc::clone(&counter);
        handles.push(thread::spawn(move || {
            let mut num = counter.lock().unwrap();
            *num += 1;
        }));
    }
    for handle in handles {
        handle.join().unwrap();
    }
    let x = counter.lock().unwrap();
    assert!(*x.deref() == 10);
    //assert!(&counter.lock().unwrap() == 10); ??
}

fn _my_first_thread() {
    let handle = thread::spawn(|| {
        for i in 0..10 {
            println!("{i}");
            thread::sleep(Duration::from_millis(1));
        }
    });
    for i in 0..10 {
        println!("{i}");
        thread::sleep(Duration::from_millis(1));
    }
    handle.join().unwrap();
}

fn _my_second_thread() {
    let v = vec![1, 2, 3];
    let handle = thread::spawn(move || {
        // `move` moves `v` to the thread.
        println!("Here's a vector: {:?}", v);
    });
    // So there's no v here, it has been given to the thread.
    handle.join().unwrap();
}

pub fn main() {
    using_mpsc();
    using_mutex();
    serialize();
    parsing();
}
