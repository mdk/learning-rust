fn seconds_in_a_year() -> u64 {
    60 * 60 * 24 * 365
}

fn approx_pi() {
    let pi: f64 = 8958937768937. / 2851718461558.;

    assert!(pi > 3.0);
}

pub fn main() {
    assert!(seconds_in_a_year() > 3600);
    approx_pi();
}
