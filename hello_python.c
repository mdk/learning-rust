#define PY_SSIZE_T_CLEAN
#include <Python.h>

int hello_python()
{
    Py_Initialize();
    PyRun_SimpleString("from time import time,ctime\n"
                       "print('Today is', ctime(time()), 'from cpython')\n");
    if (Py_FinalizeEx() < 0) {
        printf("Failure during FinalizeEx");
        return -1;
    }
    return 0;
}
